/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.abstract_animal;

/**
 *
 * @author acer
 */
public class testAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        h1.speak();
        h1.sleep();
        System.out.println("h1 is animal : " + (h1 instanceof Animal));
        System.out.println("h1 is land animal : " + (h1 instanceof LandAnimal));
        Animal a1 = h1;
        System.out.println("a1 is land animal : " + (a1 instanceof LandAnimal));
        System.out.println("a1 is poultry  : " + (a1 instanceof Poultry));
        System.out.println("a1 is reptile : " + (a1 instanceof Reptile));
        System.out.println("a1 is aquatic animals : " + (a1 instanceof AquaticAnimals));
        System.out.println("");
        System.out.println("----------------------------------------------------");

        System.out.println("");
        Dog d1 = new Dog("Puck");
        d1.eat();
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        System.out.println("d1 is animal : " + (d1 instanceof Animal));
        System.out.println("d1 is land animal : " + (d1 instanceof LandAnimal));
        Animal a2 = d1;
        System.out.println("a2 is land animal : " + (a2 instanceof LandAnimal));
        System.out.println("a2 is poultry  : " + (a2 instanceof Poultry));
        System.out.println("a2 is reptile : " + (a2 instanceof Reptile));
        System.out.println("a2 is aquatic animals : " + (a2 instanceof AquaticAnimals));
        System.out.println("");
        System.out.println("----------------------------------------------------");

        System.out.println("");
        Cat c1 = new Cat("Kee");
        c1.eat();
        c1.walk();
        c1.run();
        c1.speak();
        c1.sleep();
        System.out.println("c1 is animal : " + (c1 instanceof Animal));
        System.out.println("c1 is land animal : " + (c1 instanceof LandAnimal));
        Animal a3 = c1;
        System.out.println("a3 is land animal : " + (a3 instanceof LandAnimal));
        System.out.println("a3 is poultry  : " + (a3 instanceof Poultry));
        System.out.println("a3 is reptile : " + (a3 instanceof Reptile));
        System.out.println("a3 is aquatic animals : " + (a3 instanceof AquaticAnimals));
        System.out.println("");
        System.out.println("----------------------------------------------------");

        System.out.println("");
        Crocodile c2 = new Crocodile("Ham");
        c2.eat();
        c2.walk();
        c2.crawl();
        c2.speak();
        c2.sleep();
        System.out.println("c2 is animal : " + (c2 instanceof Animal));
        System.out.println("c2 is Readable : " + (c2 instanceof Reptile));
        Animal a4 = c2;
        System.out.println("a4 is land animal : " + (a4 instanceof LandAnimal));
        System.out.println("a4 is poultry  : " + (a4 instanceof Poultry));
        System.out.println("a4 is reptile : " + (a4 instanceof Reptile));
        System.out.println("a4 is aquatic animals : " + (a4 instanceof AquaticAnimals));
        System.out.println("");
        System.out.println("----------------------------------------------------");

        System.out.println("");
        Snake s1 = new Snake("Jo");
        s1.eat();
        s1.walk();
        s1.crawl();
        s1.speak();
        s1.sleep();
        System.out.println("s1 is animal : " + (s1 instanceof Animal));
        System.out.println("s1 is reptile : " + (s1 instanceof Reptile));
        Animal a5 = s1;
        System.out.println("a5 is land animal : " + (a5 instanceof LandAnimal));
        System.out.println("a5 is poultry  : " + (a5 instanceof Poultry));
        System.out.println("a5 is reptile : " + (a5 instanceof Reptile));
        System.out.println("a5 is aquatic animals : " + (a5 instanceof AquaticAnimals));
        System.out.println("");
        System.out.println("----------------------------------------------------");
    
        System.out.println("");
        Fish f1 = new Fish("Toe");
        f1.eat();
        f1.walk();
        f1.swim();
        f1.speak();
        f1.sleep();
        System.out.println("f1 is animal : " + (f1 instanceof Animal));
        System.out.println("f1 is aquatic animals : " + (f1 instanceof AquaticAnimals));
        Animal a6 = f1;
        System.out.println("a6 is land animal : " + (a6 instanceof LandAnimal));
        System.out.println("a6 is poultry  : " + (a6 instanceof Poultry));
        System.out.println("a6 is reptile : " + (a6 instanceof Reptile));
        System.out.println("a6 is aquatic animals : " + (a6 instanceof AquaticAnimals));
        System.out.println("");
        System.out.println("----------------------------------------------------");
        
        System.out.println("");
        Crab c3 = new Crab("Mr.Crab");
        c3.eat();
        c3.walk();
        c3.swim();
        c3.speak();
        c3.sleep();
        System.out.println("c3 is animal : " + (c3 instanceof Animal));
        System.out.println("c3 is aquatic animals : " + (c3 instanceof AquaticAnimals));
        Animal a7 = c3;
        System.out.println("a7 is land animal : " + (a7 instanceof LandAnimal));
        System.out.println("a7 is poultry  : " + (a7 instanceof Poultry));
        System.out.println("a7 is reptile : " + (a7 instanceof Reptile));
        System.out.println("a7 is aquatic animals : " + (a7 instanceof AquaticAnimals));
        System.out.println("");
        System.out.println("----------------------------------------------------");
    
        System.out.println("");
        Bat b1 = new Bat("Tom");
        b1.eat();
        b1.walk();
        b1.fly();
        b1.speak();
        b1.sleep();
        System.out.println("b1 is animal : " + (b1 instanceof Animal));
        System.out.println("b1 is poultry : " + (b1 instanceof Poultry));
        Animal a8 = b1;
        System.out.println("a8 is land animal : " + (a8 instanceof LandAnimal));
        System.out.println("a8 is poultry  : " + (a8 instanceof Poultry));
        System.out.println("a8 is reptile : " + (a8 instanceof Reptile));
        System.out.println("a8 is aquatic animals : " + (a8 instanceof AquaticAnimals));
        System.out.println("");
        System.out.println("----------------------------------------------------");
    
        
        System.out.println("");
        Bird t3 = new Bird("Zeza");
        t3.eat();
        t3.walk();
        t3.fly();
        t3.speak();
        t3.sleep();
        System.out.println("b1 is animal : " + (t3 instanceof Animal));
        System.out.println("b1 is poultry : " + (t3 instanceof Poultry));
        Animal a9 = t3;
        System.out.println("a9 is land animal : " + (a9 instanceof LandAnimal));
        System.out.println("a9 is poultry  : " + (a9 instanceof Poultry));
        System.out.println("a9 is reptile : " + (a9 instanceof Reptile));
        System.out.println("a9 is aquatic animals : " + (a9 instanceof AquaticAnimals));
        System.out.println("");
        System.out.println("----------------------------------------------------");
    }
}
